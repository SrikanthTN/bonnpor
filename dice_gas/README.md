This version of DICE is analogous to the version of DICE in "dice_particle" folder except this is modified to handle gas physics and  
can be used to generate initial conditions for hydrodynamical run of a disk galaxy.

Please be cautious about the "path/to/your/directory" during installation.
 

Installation instructions: 
Disclaimer: 
This Wiki page intends to guide you in the process of compiling and installing the DICE software. 
Before trying to install DICE, please check that the following software/libraries are installed on your system: CMake, GSL, FFTW3 

Installing dependencies:
It is preferable to install your local version of GSL and FFTW, especially if your are running on a cluster. 
First, make sure you have a local folder in your $HOME:  
cd ~  
mkdir local  

Add to your .bashrc(linux systems) or .profile (darwin systems):  
export PATH=$HOME/local/bin:$PATH  

To perform a local installation of the libraries, execute the following command lines:    
wget http://mirror.switch.ch/ftp/mirror/gnu/gsl/gsl-2.4.tar.gz  
tar -zxvf gsl-2.4.tar.gz  
cd gsl-2.4  
./configure --prefix=$HOME/local --enable-shared  
make  
make install  
cd ..  
wget http://www.fftw.org/fftw-3.3.5.tar.gz  
tar -zxvf fftw-3.3.5.tar.gz  
cd fftw-3.3.5  
./configure --prefix=$HOME/local --enable-threads --enable-shared  
make  
make install  
cd ..  

Compile & Install
You can checkout the latest version in the Git repository by typing:  
 
1) git clone git@bitbucket.org:SrikanthTN/bonnpor.git  

2) git clone https://SrikanthTN@bitbucket.org/SrikanthTN/bonnpor.git  

Please then cut the disc subfolder in the init_conditions folder and paste it into your home directory. You may then delete the MOND folder if you wish.  

The DICE package comes with the CMake cross-platform build system. So technically, you don�t have to worry so much about the compilation. Make sure you have cmake installed by typing:  

cmake --version
If your version is older than cmake 2.6, you will have to update your system to a more recent version of cmake. To generate the makefile, type:  

 
cd dice_gas  
mkdir build  
cd build   
cmake ..  
If no errors are detected through this step, compile the code and install the code like this:  
make  
make install  

By default, DICE is installed in $HOME/local/disc, but you can specify a different installation directory using the flag -DCMAKE_INSTALL_PREFIX=/install/path.  
A cmake macro is implemented to locate standard installations of the required libraries. Nevertheless, if you installed them in a different way, use the following keywords to help cmake locating these libraries:  
cmake .. -DGSL_PATH=/path/to/gsl -DFFTW3_PATH=/path/to/fftw3 -DFFTW3_THREADS_PATH=/path/to/fftw3_threads  

Furthermore, it is possible to specify the compiler flags to cmake. For example, debug options for the icc compiler:  
cmake .. -DCMAKE_C_FLAGS="-g -O2 -traceback -check=uninit -debug all -ftz -ftrapuv"  

Once installed, think about adding to the PATH environment variable the location of the DICE binary. If you use bash and under standard installation process, add to your .bashrc (or .profile for OSX systems) :  
export PATH=$HOME/local/bin:$PATH  

Optionally, if you want the code to run faster, you can use the threading abilities of the FFTW library for the gravitational potential computation and some openMP optimisations. In this case, and assuming that you have installed the treaded version of FFTW3, then replace the previous commands by:  

cmake .. -DENABLE_THREADS=ON  
make make install CMake will automatically look for the fftw3_threads library, and link it. Compilation flags can be specified with the keyword -DCMAKE_C_FLAGS  

If you have some errors referring to unknown reference intel_fast_memset, it probably means that FFTW was compiled with intel icc compiler with specific optimisation flags. Thus, you will need icc to compile DICE.    


Reference:  
http://adsabs.harvard.edu/abs/2016ascl.soft07002P  

The QUMOND analogue of the Toomre disk stability condition is obtained from https://arxiv.org/abs/1808.10545.  
The algorithm is based on applying QUMOND via the algebraic MOND approximation. The critical MOND adjustments are in the dice_vel.c file.  

For queries, please contact Indranil Banik at indranilbanik1992@gmail.com.   