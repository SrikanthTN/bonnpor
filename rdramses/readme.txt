#################################################################
# rdramses in singular and parallel usage for hydro-simulations #
#################################################################

This folder contains the following three files:
readme.txt (this file)
Makefile
rdramses_itib2019.f90 (the primary algorithm)

rdramses is a tool to convert the raw-data (binary) from ramses (Teyssier 2004) or phantom of ramses (POR, Lueghausen 2015) to ASCII.
The original version was developed by Florent Renaud and can be applied to either particle-only simulations, hydro-simulation and magneto-hydro-simulations.
Issues with this version have been resolved by Ingo Thies, Indranil Banik, and Nils Wittenburg, so the algorithm is now rated to work.
The description, the download of the original version, the syntax and a short How-To can be found at:

http://www.astro.lu.se/~florent/rdramses.php ,

where also several features of the tool are presented that were not tested for this version.

This version focuses on the conversion of data from a hydro-simulation with or without mpi (parallel processing).
The implementation of parallelisation is quite simple here, as one needs to specify manually how the simulation box is divided.
The code then calculates the corresponding boundaries on the master-core and this sends the information to the rest of the cores, e.g:

mpiexec -n 16 /path/to/rdramses -inp /path/to/POR/outputfile -hydro -option1 -option2 -ncol 4 -nrow 4

'ncol' (x-axis) and 'nrow' (y-axis) specify how the 'xy'-plane of the simulation box is divided and this is then carried through the z-axis, 
meaning that the simulation volume is divided into ncol * nrow rectangles.
Important here is that nrow * ncol needs to be equal to the number of cores one is using in total ('-n 16' from the example).
The algorithm contains a safety catch to check if this is the case.
All options, like '-hydro', can be used, but only '-hydro' '-cen' (puts the origin at the centre of the simulation box) were tested for this version.
To use the code on a single core, one just leaves out the mpi-parts, e.g.

./path/to/rdramses -inp /path/to/POR/outputfile -hydro -option1 -option2

################### Makefile ###################
In the Makefile one specifies the sourcepath (SRCPATH), path for the executable (EXEPATH), the compiler and different libraries.
To compile rdramses with mpi, the command

make rdramses_mpi 

has to be executed. All other versions with their respective commands are listed in the Makefile.

################### Output ###################
The output is generated where one executes rdramses and one output (or several in the case of the mpi version) for one original POR output is generated.
They are named as follows, e.g.

rdr_00001_l9_centered_cpu000001.hydro

The first '00001' is the number of the original POR output, 'l9' shows the maximum level of refinement of the simulation, 'cpu00001' shows the core-number 
(this part is missing for runs without mpi) and '.hydro' shows the main option, e.g. 'hydro' for the -hydro option or 'part' for the -part option etc.
The hydro output contains, x y z vx vy vz rho level m temp.
[position] = kpc
[velocity] = km/s
[density] = H/cm^3
[level] = unitless, as it just shows the refinement-level of the gas-cell
[mass] = Msun
[temperature] = K 

The units are physical units and the conversion is done automatically in this version. However, if one wants to handle the unit conversions manually,
one needs to comment out line number 1513 in the rdramses_itib.f90 file, and uncomment line 1512. Line 1512 gives the output in default RAMSES units and one 
can then convert them manually. The algorithm has been tested for isolated galaxy runs and cosmological runs. In case one decides to let rdramses
handle the unit conversions, then depending on the type of simulation - isolated galaxy or cosmological run - one must comment or uncomment line
number 407 in the rdramses_itib.f90 file. This line is commented out in this version, in case one wants to use this on cosmological run,
it is recommended to uncomment line 407.



For batch processing the following bash script can be used. It takes a few minutes per output snapshot, so consider running over
night if many outputs are present. Given that the relative path to a simulation folder containing 100 snapshots is
"../myporsimulation/" the script it reads:

#!/bin/bash
cnt=0
cntmax=100
while [ $cnt -lt $cntmax ]
do
    ((cnt++))
    dirname="$( printf "../myporsimulation/output_%05d" $cnt)"
    echo Processing $dirname
    ~/bin/rdramses -inp $dirname -hydro -cen
done
