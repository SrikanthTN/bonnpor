module format4ramses
  use commons4ramses
  use deviates_module
  implicit none
  integer :: com_mode,dcen_mode,sort_mode,skip_mode,skip_static,boxcen_mode
  integer :: np1,np2,ncalls_splitpops
  real(kind=8) :: mp1,mp2,mstar,mstatic,dcfrac
  real(kind=8),dimension(6) :: com_rv,dcen_rv
  integer,parameter :: npmax=4001000
  logical :: com_correction_firstrun=.true.,dcen_correction_firstrun=.true.
  real(kind=8) :: com_cpu0,com_cpu1

contains

  subroutine read_ramses_info(infofile,nthreads,ncharin)
    character(len=200) :: infofile
    character(len=15) :: chdum
    character(len=5) :: ncharin
    real(kind=8) :: tsu
    integer :: i,iu=98,nthreads
    open(iu,file=trim(infofile))
    if (nthreads<=0) then
       read(iu,'(a15,i9)') chdum,nthreads
       print *,'Assuming ',nthreads,' CPU cores for output',ncharin
    else
       read(iu,*)
    endif
    do i=1,6
       read(iu,*)
    enddo
    read(iu,'(a15,e21.15)') chdum,boxlength
    read(iu,'(a15,e21.15)') chdum,tsu
    if (boxlen_in>0.d0) boxlength=boxlen_in!switch for optional manual setting
    do i=1,6
       read(iu,*)
    enddo
    read(iu,'(a15,e21.15)') chdum,slu
    read(iu,'(a15,e21.15)') chdum,sdu
    read(iu,'(a15,e21.15)') chdum,stu
    close(iu)
    tnow_myr=tsu*stu/3.15576d13
    return
  end subroutine read_ramses_info

  subroutine read_ramses_part(fileid,nthreads,nplive,all_part,id_part)
    character(len=200) :: fileid,infile
    character(len=5) :: nchar
    integer :: i,j,iu,ithread,nthreads,ndata,ndim,idum,npart,ierr
    integer :: nplive,npfull,islive
    real(kind=8) :: m,ddum
    real(kind=8),dimension(3) :: v
    real(kind=8),dimension(11,npmax) :: all_part
    real(kind=8),allocatable,dimension(:,:) :: all_rdata
    integer,allocatable,dimension(:) :: all_idata
    integer,dimension(npmax) :: id_part
    real(kind=8),allocatable,dimension(:)::xdp
    integer,allocatable,dimension(:)::idp,ill
    logical :: file_exists
    npfull=0!all particles
    nplive=0!live particles
    do ithread=1,nthreads
       iu=10+ithread
       write(nchar,'(i5.5)') ithread
       infile=trim(fileid)//nchar
!       print *,'ithread,nchar =',ithread,'; "',trim(infile),'"'
       open(iu,file=trim(infile),form='unformatted',status='old',iostat=ierr)
       if (ierr/=0) then
          close(iu)
          print '(a,a,a)','File "',trim(infile),'" does not exist (normal if end)'
          print '(a,i2.2,a,i2)','iu',iu,': ierr =',ierr
          call close_program
          !          exit
       endif
       read(iu) idum
       read(iu) ndim
       read(iu) npart
       read(iu) idum
       read(iu) idum
       read(iu) ddum
       read(iu) ddum
       read(iu) idum
       ndata=npart
!       print *,'npart =',npart
       allocate(xdp(1:npart))
       allocate(idp(1:npart))
       allocate(ill(1:npart))
       allocate(all_rdata(11,1:npart))
       allocate(all_idata(1:npart))
!       ndata=0
!       do i=1,npart
!          read(iu,end=7) ddum
!          print *,'i =',i
!          ndata=ndata+1
!       enddo
!7      continue
!       print *,'ndata =',ndata
!       cycle
! positions
       do j=1,ndim
          read(iu) xdp
          do i=1,npart
!             read(iu) all_rdata(j,i)
             all_rdata(j,i)=xdp(i)*scalr
!             print *,'j,i =',j,i,all_indata(j,i)
          enddo
       enddo
! velocities
       do j=1,ndim
          read(iu) xdp
          do i=1,npart
!             read(iu) all_rdata(j+3,i)
             all_rdata(j+3,i)=xdp(i)*scalv
          enddo
       enddo
!       print *,'scalv =',scalv
! accelerations !NEW!
       if (existacc) then
          do j=1,ndim
             read(iu) xdp
             do i=1,npart
                all_rdata(j+7,i)=xdp(i)*scalacc
             enddo
          enddo
       endif
! mass
       read(iu) xdp
       do i=1,npart
!          read(iu) all_rdata(7,i)
          all_rdata(7,i)=xdp(i)*scalm
       enddo
! ID
       read(iu) idp
       do i=1,npart
!          read(iu) id_part(i)
          all_idata(i)=idp(i)
       enddo
       if (usetp) then
! timestamp (birth epoch) !UNDER CONSTRUCTION!
          read(iu,end=7,err=8) ill
          read(iu,end=7,err=8) xdp
          do i=1,npart
             if (xdp(i)>0.d0) then
                all_rdata(11,i)=xdp(i)*scaltp
             else
                all_rdata(11,i)=-1.d15!older than the universe is safe
             endif
!                if (xdp(i)>0.d0) print *,'**** tp>0. ****'
          enddo
       endif
       goto 9
7      continue
       !print *,'End of file --> close'
       goto 9
8      print *,'File read error -->close'
9      close(iu)
       npfull=npfull+npart
       print *,'Read thread No.',ithread,'; particle number so far:',npfull, ' (added ',npart,')'
       do i=1,npart
          v(1:3)=all_rdata(4:6,i)
          m=all_rdata(7,i)
          call detect_staticparts(v,m,islive)
          if (islive==0) cycle
          nplive=nplive+1
          all_part(1:11,nplive)=all_rdata(1:11,i)
          id_part(nplive)=all_idata(i)
       enddo
       !print *,'++++++++++++++++++ nplive = ',nplive
       deallocate(xdp)
       deallocate(idp)
       deallocate(ill)
       deallocate(all_rdata)
       deallocate(all_idata)
    enddo
    if (nplive==0) then
       print *,'Something has gone wrong -- no live particles found.'
       print *,'Maybe increase static mass threshold?'
       stop
    endif
  end subroutine read_ramses_part

  subroutine write_ramses_part(outfile,gpauxfile,nptot,all_part,id_part,all_live)
    character(len=200) :: outfile,gpauxfile
    character(len=5) :: nchar
    integer :: i,j,ii,ndim,idum
    integer :: nptot
    integer,dimension(npmax) :: status_part
    real(kind=8),dimension(11,npmax) :: all_part
!    real(kind=8),dimension(3,npmax) :: all_acc
    real(kind=8) :: ddum,vdum(3),evel(3),el(3)
    integer,dimension(npmax) :: id_part,all_live
    call set_boxcenter
    call com_correction(nptot,all_part,evel,el)
    call rotate_ramses_part(nptot,all_part,evel,el)
    if (sort_mode==1) then
       call rqsort(nptot,id_part,ai_sorted)
    else
       do i=1,nptot
          ai_sorted(i)=i
       enddo
    endif
    call trim_ramses_part(nptot,all_part,status_part)
    open(90,file=trim(outfile))
    do i=1,nptot
       if (skip_mode>0.and.i/=skip_mode*(i/skip_mode)) cycle
       if (sort_mode==1) then
          ii=ai_sorted(i)
       else
          ii=i
       endif
       !if (skip_static>=1.and.all_live(ii)==0) cycle
       if (status_part(ii)==0) then
          if (flgtrim==2) then
             ddum=boxlength-boxrim
             call uniform3d(ddum,boxcenter,vdum)
             all_part(1:3,ii)=vdum
          else
             cycle
          endif
       endif
       if (boxcen_mode>=1) then
          all_part(1:3,ii)=all_part(1:3,ii)-boxcenter(1:3)
       endif
       if (usetp) then
          write(90,1090) all_part(1:7,ii),id_part(ii),all_part(8:10,ii),all_part(11,ii),tnow_myr-all_part(11,ii)
       else if (useacc) then
          write(90,1090) all_part(1:7,ii),id_part(ii),all_part(8:10,ii)
       else
          !This is the really important line for normal options (write ASCII files without acceleration and birth time).
          write(90,1090) all_part(1:7,ii),id_part(ii)
       endif
!       if (i>=1199000) write(*,1090) all_part(1:7,ii),id_part(ii)
    enddo
    close(90)
    open(91,file=trim(gpauxfile))
    write(91,*) 'tnow_myr=',tnow_myr
    write(91,'(a,i5,a)') 'tnow_lbl="',nint(tnow_myr),' Myr"'
    close(91)
1090 format(7(1x,e17.9),2x,i8,3(1x,e17.9),2(1x,e17.9))
  end subroutine write_ramses_part

  subroutine detect_staticparts(v,m,islive)
    implicit none
    integer :: nptot,i,islive
    real(kind=8),dimension(3) :: v
    real(kind=8) :: vsq,m
    vsq=v(1)**2+v(2)**2+v(3)**2
    if (skip_static>0.and.((mstatic>0.d0.and.m>=mstatic).or.vsq==0.d0)) then
       islive=0
    else
       islive=1
    endif
    return
  end subroutine detect_staticparts
  
!  subroutine find_staticparts(nptot,all_part,all_live)
!    implicit none
!    integer :: nptot,i
!    integer,dimension(npmax) :: all_live
!    real(kind=8),dimension(11,npmax) :: all_part
!    real(kind=8) :: vsq,m
!    do i=1,nptot
!       all_live(i)=1
!       if (skip_static<=0) cycle
!       m=all_part(7,i)
!       vsq=all_part(4,i)**2+all_part(5,i)**2+all_part(6,i)**2
!       if ((mstatic>0.d0.and.m>=mstatic).or.vsq==0.d0) then
!          all_live(i)=0
!       endif
!    enddo
!    return
!  end subroutine find_staticparts
  
  subroutine split_populations(nptot,all_part,id_part)
    !requires usetp to be true!
    implicit none
    integer :: nptot,i,i1,i2
    integer,dimension(npmax) :: id_part
    real(kind=8),dimension(11,npmax) :: all_part
    real(kind=8) :: accdum
    logical :: is_star(npmax)
    ncalls_splitpops=ncalls_splitpops+1
    nstars=0
    do i=1,nptot
       is_star(i)=all_part(11,i)>0.d0!positive age
       if (is_star(i)) nstars=nstars+1
    enddo
    if (ndm<0) then
       ndm=nptot-nstars
       print *,'ndm,nstars =',ndm,nstars
    endif
    i1=0
    i2=ndm
    ! re-write id_part to remove RAMSES' index clutter
    do i=1,nptot
       if (usetp) then
          if (is_star(i)) then!advance star index (appended to dm particles block)
             i2=i2+1
             !          id_part(i)=i2
             id_part(i)=id_part(i)+ndm
          else!advance dm index
             i1=i1+1
             !          id_part(i)=i1
          endif
       endif
       !debug: warning about zero accelerations (maybe normal for newborn stars)
!       if (ncalls_splitpops>1) then
!          accdum=sqrt(all_part(8,i)**2+all_part(9,i)**2+all_part(10,i)**2)
!          if (accdum==0.d0) print *,'Zero acceleration found at i =',i
!       endif
    enddo
    return
  end subroutine split_populations

  subroutine com_correction(nptot,all_part,evel,el)
    integer :: nptot,i
    !integer,dimension(npmax) :: all_live
    real(kind=8),dimension(11,npmax) :: all_part
    real(kind=8),dimension(3) :: posc,velc,pos2,epos,evel,el
    real(kind=8) :: mtot
    !print *,'>>>>>>>>>>>>>>>> com_mode = ',com_mode,nptot
    if (com_mode==0.and.dcen_mode==0) return
    if (com_correction_firstrun.or.com_mode>0.or.dcen_mode>0) then
       !in dcen_mode=1 velocity is taken from CoM
       com_rv=0.d0
       mtot=0.d0
       do i=1,nptot
          !if (skip_static>=2.and.all_live(i)==0) cycle!skip staticparts
          com_rv(1:6)=com_rv(1:6)+all_part(1:6,i)*all_part(7,i)!no longer assume equal masses here
          mtot=mtot+all_part(7,i)
!          print *,'mtot =',mtot
       enddo
       com_rv(1:6)=com_rv(1:6)/mtot
       com_correction_firstrun=.false.
    endif
    evel=(/0.d0,1.d0,0.d0/)
    el=(/0.d0,0.d0,1.d0/)
    if (dcen_mode>0) then
       call dcen_correction(nptot,all_part,posc,velc)
       dcen_correction_firstrun=.false.
       pos2(1:3)=posc(1:3)-boxref(1:3)
       epos(1:3)=pos2(1:3)/sqrt(pos2(1)**2+pos2(2)**2+pos2(3)**2)
       if (dcen_mode==1) then
          velc(1:3)=com_rv(4:6)
       endif
       evel(1:3)=velc(1:3)/sqrt(velc(1)**2+velc(2)**2+velc(3)**2)
       call pvec3(epos,evel,el)
       !print '(a,3(1x,f16.8))','boxc(1:3)   =',boxref(1:3)
       !print '(a,3(1x,f16.8))','pos2(1:3)   =',pos2(1:3)
       !print '(a,3(1x,f16.8))','velc(1:3)   =',velc(1:3)
       print '(a,3(1x,f16.8))','epos(1:3)   =',epos(1:3)
       print '(a,3(1x,f16.8))','evel(1:3)   =',evel(1:3)
    endif
    print '(a,3(1x,f16.8))','COM_RV(1:3) =',com_rv(1:3)
    print '(a,3(1x,f16.8))','COM_RV(4:6) =',com_rv(4:6)
    do i=1,nptot
       if (dcen_mode>0) then
          all_part(1:3,i)=all_part(1:3,i)-posc(1:3)
       else if (abs(com_mode)==1.or.abs(com_mode)==3) then
          all_part(1:3,i)=all_part(1:3,i)-com_rv(1:3)
       endif
       if (abs(com_mode)==2.or.abs(com_mode)==3) then
          all_part(4:6,i)=all_part(4:6,i)-com_rv(4:6)
       endif
    enddo
    return
  end subroutine com_correction
  
  subroutine dcen_correction(nptot,all_part,posc,velc)
!EXPERIMENTAL!
    integer :: i,ii,nptot
    real(kind=8) :: mthres=1.d99!safely take all particle masses
    real(kind=8),dimension(11,npmax) :: all_part
    real(kind=8),dimension(nptot) :: om
    real(kind=8),dimension(3,nptot) :: opos,ovel
    real(kind=8),dimension(3) :: posc,velc
    real(kind=8),dimension(6) :: dcen_rv
    real(kind=8) :: rdens,avdens
!    n=nhi-nlo+1
    do i=1,nptot
       om(i)=all_part(7,i)
       opos(1:3,i)=all_part(1:3,i)
       ovel(1:3,i)=all_part(4:6,i)
    enddo
    !print '(a,i1,a)','Calculation of density center ',igroup,'...'
    !print '(a,3(1x,i7))','nptot =',nptot
!    print *,'opos =',opos(1:3,1)
    call densitycenter(3,om,opos,nptot,dcfrac,mthres,posc,rdens,avdens,0)
    call densitycenter(3,om,ovel,nptot,dcfrac,mthres,velc,rdens,avdens,0)
    return
  end subroutine dcen_correction

  subroutine rotate_ramses_part(nptot,all_part,evel,el)
    integer :: nptot,i
    real(kind=8) :: ddum,eps=1.d-12
    real(kind=8) :: x,y,z,r,theta,phi,lx,ly,lz,rl,thel,phil
    real(kind=8),dimension(11,npmax) :: all_part
    real(kind=8),dimension(3) :: p0,v0,p1,v1,p2,v2,evel,el,ev1
    ddum=abs(d_rot)+abs(e_rot)+abs(f_rot)
    !a,b,c_rot: Euler angles for first (automatic) rotation
    !d,e,f_rot: Euler angles for second (manual) rotation
    if (ddum<eps.and.autorot_mode<=0) return!do nothing if angles are (almost) zero
    if (autorot_mode==1) then
       if (abs(rot_period)>eps) then
          phi=tnow_myr*2.d0*pi/rot_period
          b_rot=0.d0
       else
          x=com_rv(1)
          y=com_rv(2)
          z=com_rv(3)
          call ct2sph(x,y,z,r,theta,phi)
          b_rot=theta-0.5d0*pi
       endif
       a_rot=-phi
       c_rot=0.d0
    else if (autorot_mode==2) then
       !correction by velocity vector, no tilt (by y axis) correction
       x=evel(1)
       y=evel(2)
       z=evel(3)
       call ct2sph(x,y,z,r,theta,phi)
       a_rot=0.5d0*pi-phi
       b_rot=theta-0.5d0*pi!note that b_rot aims to get theta=pi/2, i.e. latitude=0
       c_rot=0.d0
    else if (autorot_mode==3) then
       !correction by angular momentum vector --> z and velocity vector --> y
       lx=el(1)
       ly=el(2)
       lz=el(3)
       call ct2sph(lx,ly,lz,rl,thel,phil)
       !a_rot=0.5d0*pi-phil
       a_rot=-phil
       b_rot=-thel
       call rotzyz(evel,ev1,a_rot,b_rot,0.d0)
       !ev1=evel!debug
       x=ev1(1)
       y=ev1(2)
       z=ev1(3)
       call ct2sph(x,y,z,r,theta,phi)
       c_rot=0.5d0*pi-phi
       !c_rot=-phi
       !print *,'el(1:3)     = ',el
       !print *,'thel, phil  = ',thel,phil
       !print *,'theta, phi  = ',theta,phi
       !print *,'thel, phil °= ',thel*180.d0/pi,phil*180.d0/pi
       !print *,'theta, phi °= ',theta*180.d0/pi,phi*180.d0/pi
    endif
    do i=1,nptot
       p0(1:3)=all_part(1:3,i)
       v0(1:3)=all_part(4:6,i)
       if (autorot_mode>=1) then
          call rotzyz(p0,p1,a_rot,b_rot,c_rot)
          call rotzyz(v0,v1,a_rot,b_rot,c_rot)
       else
          p1=p0
          v1=v0
       endif
       if (ddum>eps) then
          call rotzxz(p1,p2,d_rot,e_rot,f_rot)
          call rotzxz(v1,v2,d_rot,e_rot,f_rot)
       else
          p2=p1
          v2=v1
       endif
       all_part(1:3,i)=p2(1:3)
       all_part(4:6,i)=v2(1:3)
    enddo
    return
  end subroutine rotate_ramses_part

  subroutine trim_ramses_part(nptot,all_part,status_part)
    integer :: i,nptot,nout
    integer,dimension(npmax) :: status_part
    real(kind=8),dimension(11,npmax) :: all_part
    real(kind=8) :: dquad
    logical :: isout=.false.
    status_part=0
    nout=0
    !call set_boxcenter
    do i=1,nptot
       dquad=max(abs(all_part(1,i)-boxcenter(1)),&
                 abs(all_part(2,i)-boxcenter(2)),&
                 abs(all_part(3,i)-boxcenter(3)))
       isout=dquad>=boxlength/2.d0-boxrim
       if (isout.and.flgtrim>=1) then
          status_part(i)=0
          nout=nout+1
!          print *,'pos =',all_part(1:3,i)-boxcenter(1:3),dquad
!          print *,'bc =',boxcenter
       else
          status_part(i)=1
       endif
    enddo
    if (nout>0) then
       if (flgtrim==2) then
          print *,'particles outside box - redistributed: ',nout
       else
          print *,'particles outside box - skipped: ',nout
       endif
    endif
    return
  end subroutine trim_ramses_part

!**********************************************************************
!     PIKAIA-Version of Quicksort
!     made F90-ready and converted to double precision by I. Thies in 2013,
!     adopted for format4ramses (integer array) in 2015
  subroutine rqsort(n,a,p)
!======================================================================
!     Return integer array p which indexes array a in increasing order.
!     Array a is not disturbed.  The Quicksort algorithm is used.
!
!     B. G. Knapp, 86/12/23
!
!     Reference: N. Wirth, Algorithms and Data Structures,
!     Prentice-Hall, 1986
!======================================================================
    implicit none

!     Input:
    integer :: n
!    real(kind=8) :: a(n)
    integer :: a(n)

!     Output:
    integer :: p(n)

!     Constants
    integer,parameter :: LGN=32, Q=11
!        (LGN = log base 2 of maximum n;
!         Q = smallest subfile to use quicksort on)

!     Local:
    real(kind=8)      x
    integer :: stackl(LGN),stackr(LGN),s,t,l,m,r,i,j

!     Initialize the stack
    stackl(1)=1
    stackr(1)=n
    s=1

!     Initialize the pointer array
    do 1 i=1,n
       p(i)=i
1   enddo

2   if (s.gt.0) then
       l=stackl(s)
       r=stackr(s)
       s=s-1

3      if ((r-l).lt.Q) then

!           Use straight insertion
          do 6 i=l+1,r
             t = p(i)
             x = a(t)
             do 4 j=i-1,l,-1
                if (a(p(j)).le.x) goto 5
                p(j+1) = p(j)
4            enddo
             j=l-1
5            p(j+1) = t
6         enddo
       else

!           Use quicksort, with pivot as median of a(l), a(m), a(r)
          m=(l+r)/2
          t=p(m)
          if (a(t).lt.a(p(l))) then
             p(m)=p(l)
             p(l)=t
             t=p(m)
          endif
          if (a(t).gt.a(p(r))) then
             p(m)=p(r)
             p(r)=t
             t=p(m)
             if (a(t).lt.a(p(l))) then
                p(m)=p(l)
                p(l)=t
                t=p(m)
             endif
          endif

!           Partition
          x=a(t)
          i=l+1
          j=r-1
7         if (i.le.j) then
8            if (a(p(i)).lt.x) then
                i=i+1
                goto 8
             endif
9            if (x.lt.a(p(j))) then
                j=j-1
                goto 9
             endif
             if (i.le.j) then
                t=p(i)
                p(i)=p(j)
                p(j)=t
                i=i+1
                j=j-1
             endif
             goto 7
          endif

!           Stack the larger subfile
          s=s+1
          if ((j-l).gt.(r-i)) then
             stackl(s)=l
             stackr(s)=j
             l=i
          else
             stackl(s)=i
             stackr(s)=r
             r=j
          endif
          goto 3
       endif
       goto 2
    endif
    return
  end subroutine rqsort

end module format4ramses
