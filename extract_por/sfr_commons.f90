module sfr_commons
  use commons4ramses
  implicit none
  integer :: sfr_index_time=-1
  integer,parameter :: sfr_ntmax=10000
  real(kind=8) :: sfr_now,t_sfr
  !real(kind=8),dimension(0:sfr_ntmax) :: sfr_all_mtot,sfr_all_diffm,sfr_all
  
contains

  subroutine sfr_advance
    !This routine advanced the star-formation history at every call.
    integer :: i
    real(kind=8) :: difft,diffm,mtot
    sfr_index_time=sfr_index_time+1
    mtot=0.d0
    do i=1,ndata
       mtot=mtot+all_data(7,i)
    enddo
    mtot_now=mtot
    difft=tnow_myr-tprev_myr
    diffm=mtot_now-mtot_prev
    sfr_now=diffm/difft
    !sfr_all_mtot(sfr_index_time)=mtot_now
    !sfr_all_diffm(sfr_index_time)=diffm
    !sfr_all(sfr_index_time)=sfr_now
    mtot_prev=mtot_now
    t_sfr=0.5d0*(tnow_myr+tprev_myr)
    return
  end subroutine sfr_advance
  
end module sfr_commons
