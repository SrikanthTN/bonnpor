## BONN Phantom of Ramses 
BONNPoR is a bitbucket created by Srikanth Togere Nagesh, under the supervision of Prof Pavel Kroupa, University of Bonn, Germany. This repository hosts all the software, algorithms and tools required
to setup, run and analyse disc galaxy simulations using Phantom of Ramses. There are 5 folders :

1) dice_particle  
2) dice_gas   
3) PoR_hydro  
4) extract_por  
5) rdramses

## dice_particle ##
DICE was developed by V. Perret and later mondified by Roy Truelove, Indranil Banik and Ingo Thies to accomodate Milgromian Dynamics (MOND).  
"dice_particle" contains the mondified version of DICE software. Dice can be used to generate initial conditions for a disk galaxy, but this version of dice   
only used for a particle-only run (N-Body run only). 

## dice_gas ##
"dice_gas" contains the mondified version of the DICE software. Dice can be used to generate initial conditions for a disk galaxy, but this version of dice   
used for a hydrodynamical run, which can handle gas component of a disk. 


## PoR_hydro ##
Phantom of Ramses (PoR) was developed by Fabian Lueghausen in 2015. It is a patch to the original RAMSES by R. Teyssier. 
Phantom of RAMSES allows the user to run the RAMSES simulation with MOND. "PoR_hydro" contains the 2015 version of 
RAMSES package, which is compatible with the PoR patch. This PoR is customized by Indranil Banik and Ingo Thies for specific 
scientific goal. However, it can be used for a general purpose as well. 

## extract_por ##
"extract_por" is an data extraction tool developed by Ingo Thies. This tools is useful in extracting and analysing the PoR simulation outputs.

## rdramses##
It is also an extraction tool used to analyse hydrodynamical simulation outputs of PoR.
It was developed by Florent Renaud and modified by Indranil Banik, Ingo Thies and, Nils Wittenburg. 

## PoR_manual.pdf 
We have uploaded a manual that briefly describes the procedure to use Phantom of RAMSES software to perform a disc galaxy simulation, along with some other examples.
The manual is available on arxiv in a citable format, https://arxiv.org/abs/2101.11011 

## Cloning this repository 
One can download the entire package by clicking on "clone" on the top right corner of this page. One can just copy the command to the terminal and extract package.


## All the aforementioned software and tools are tested and are rated to work. Contact:- tnsrikanth@uni-bonn.de, indranilbanik1992@gmail.com
